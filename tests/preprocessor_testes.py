
from preprocessor import *
from sklearn.pipeline import Pipeline

s='Oi, como vai você?? @10 thiago pulo pulado pulamos pulamos pulamos'
print(s)

pipe1 = Pipeline( [('cleaning', Cleaner())])
r = pipe1.transform([s])
print(r)

pipe1 = Pipeline( [('cleaning', Cleaner(remove_accents=False, remove_alpha_numeric=False))])
r = pipe1.transform([s])
print(r)

pipe2 = Pipeline( [('stopwords', StopWords(lang='portuguese'))])
r = pipe2.transform([s])
print(r)

pipe2 = Pipeline( [('stopwords', StopWords(lang='portuguese',tokenize=True))])
r = pipe2.transform([s])
print(r)

pipe3 = Pipeline( [('cleaning', Cleaner()), ('stopwords', StopWords(lang='portuguese'))])
r = pipe3.transform([s])
print(r)

pipe3 = Pipeline( [('cleaning', Cleaner()), ('stopwords', StopWords(lang='portuguese', tokenize=True))])
r = pipe3.transform([s])
print(r)

pipe2 = Pipeline( [('stopwords', Stemmer(lang='portuguese', fit_reuse=True))])
pipe2.fit([s])
r = pipe2.transform([s])
print(r)
print(pipe2.predict(["thiag oi como  voc pul"]))


print('preproessing all!!')
preproc = Preprocessor(max_word_lenght=1)
s2 = ["""Naruto (ナルト?) é uma série de mangá escrita e ilustrada por Masashi Kishimoto. Conta a história de Naruto Uzumaki, um jovem ninja que constantemente procura por reconhecimento e sonha em se tornar Hokage, o ninja líder de sua vila. A história é dividida em duas partes, a primeira parte se passa nos anos da pré-adolescência de Naruto, e a segunda parte se passa em sua adolescência. A série é baseada em dois mangás one-shots de Kishimoto: Karakuri (1995), e que por causa dele Kishimoto ganhou uma menção honrosa no prêmio Hop Step da Shueisha no ano seguinte, e Naruto (1997). A editora Panini Comics licenciou e publicou o mangá em três versões no Brasil, e em Portugal é a editora Devir Manga quem licencia e publica o mangá. A história de Naruto continua com o filho de Naruto, Boruto Uzumaki em Boruto: Naruto Next Generations.

Os capítulos de Naruto foram publicados na revista Weekly Shōnen Jump de 1999 a 2014, com estes capítulos compilados e publicados em 72 volumes tankōbon pela editora Shueisha. O mangá foi adaptado para uma série de anime produzida pelo Studio Pierrot e Aniplex, que teve os seus 220 episódios transmitidos pela TV Tokyo no Japão de 2002 a 2007; a adaptação brasileira da série foi exibida na Cartoon Network e no SBT, e atualmente é exibido nos serviços de streaming Crunchyroll, Netflix e Claro Video. Já a adaptação portuguesa da série foi exibida na SIC Radical, SIC, SIC K e no Animax Portugal. Naruto Shippuden, a sequência da série original, estreou no Japão em 2007 e terminou em 2017, após 500 episódios. A adaptação brasileira foi exibida na PlayTV em 2015 a 2017, e atualmente é exibida nos serviços de streaming Crunchyroll, Netflix e Claro Video. A adaptação portuguesa foi exibida na SIC Radical, SIC K e no Animax Portugal. Além da série de anime, o Studio Pierrot desenvolveu onze filmes e vários OVAs. No Brasil, alguns filmes foram licenciados e exibidos dublados no país pela Viz Media. Outros produtos relacionados a Naruto como light novels, jogos eletrônicos, e cartas colecionáveis foram desenvolvidos por várias empresas.

A partir de 2017, Naruto se tornou a terceira série de mangá mais vendida na história, vendendo mais de 220 milhões de cópias em todo o mundo, onde foi publicado em 35 países fora do Japão. Os críticos elogiaram o enredo, desenvolvimento de personagens, e as cenas de luta do mangá. Os críticos também observaram que o mangá, que tem uma história coming-of-age, faz uso de referências culturais da mitologia japonesa e do confucionismo.
""",
      """
      One Punch-Man (ワンパンマン, Wanpanman?) é uma série de webcomic criada pelo autor com o pseudónimo One,[5] e é publicada desde 2009. A série rapidamente tornou-se um fenómeno viral, alcançando mais de 7,9 milhões de acessos, em junho de 2012. O nome da obra Wanpanman é uma paródia da personagem Anpanman, a palavra wanpan é uma contração de wanpanchi (one punch em língua inglesa).[6] One Punch-Man conta a história de Saitama, um super-herói extremamente poderoso, que se entediou com a ausência dos desafios nas suas lutas contra o mal e procura encontrar um oponente digno.

Uma recriação do manga digital ilustrado por Yusuke Murata, começou a ser publicada no sítio eletrónico Young Jump Web Comics em 2012. Os capítulos são compilados em volumes de formato tankōbon e publicados pela editora Shueisha. No Brasil, o manga é publicado pela editora Panini Comics desde março de 2016, e em Portugal é publicado pela editora Devir Manga desde março de 2017.

Uma adaptação em animé feita pela Madhouse, foi exibida entre outubro e dezembro de 2015.[7][8][9] Em julho de 2017 a série foi exibida na Netflix. A segunda temporada encontra-se atualmente em produção pela
      """
      ]
preproc.fit(s2)
r = preproc.transform(s2)
for i, text in enumerate(r):
    print('texto %s ---- %s' %(i,r[i]))

